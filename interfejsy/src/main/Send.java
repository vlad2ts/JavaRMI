package main;

import java.rmi.Remote;
import java.rmi.RemoteException;
import types.Spectrum;
import types.TimeHistory;

public interface Send extends Remote {
	
	public void SendSpectrum(Spectrum<Integer> a, String t ) throws RemoteException;
	public void SendTimeHistory(TimeHistory<Integer> a, String t) throws RemoteException;
    public void PrintTimeHistory(String key) throws RemoteException;
    public void PrintSpectrum(String key) throws RemoteException;

}
