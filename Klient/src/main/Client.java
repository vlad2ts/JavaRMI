package main;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import types.Spectrum;
import types.TimeHistory;

public class Client {
	
	// Initialize data
			String device = "deviceTest";
			String description = "descriptionTest";
			long date = 10250045;
			int channelNr = 3;
			String unit = "unitTest";
			double resolution = 0.01;
			double sensitivity = 0.1;
			int threshold = 50;
			byte direction = -1;
			Integer[] buffer = {1,2,3};
			int scaling = 0;
			
			//Create instances of classes with data
			TimeHistory<Integer> timeHistoryTest = new TimeHistory<Integer>(device, description, date, channelNr, unit, resolution,
					buffer, sensitivity);
			Spectrum<Integer> spectrumTest = new Spectrum<Integer>(device, description, date, channelNr, unit, resolution, buffer, scaling);

	 
private Scanner userInput = new Scanner(System.in);
String username;
Send remoteObject; // referencja do zdalnego obiektu

public static void main(String[] args) {
  	if(args.length < 1) {
		System.out.println("Usage: ChatClient <server host name>");
		System.exit(-1);
	}
  	if (System.getSecurityManager() == null) {
        System.setSecurityManager(new SecurityManager());
    }
	new Client(args[0]);


  }
public Client(String hostname) {
	System.out.println("Enter client name: ");
	if(userInput.hasNextLine())
		username = userInput.nextLine();
	Registry reg;    // rejestr nazw obiektow
	try {
		// pobranie referencji do rejestru nazw obiektow
		reg = LocateRegistry.getRegistry("192.168.198.150",1099);
		// odszukanie zdalnego obiektu po jego nazwie
		remoteObject = (Send) reg.lookup("Server");
		// wywolanie metod zdalnego obiektu
		remoteObject.SendSpectrum( spectrumTest, "ab");
		remoteObject.SendTimeHistory(timeHistoryTest, "ab");
		remoteObject.PrintSpectrum("ab");
		remoteObject.PrintTimeHistory("ab");
	} catch (RemoteException e) {
		e.printStackTrace();
	} catch (NotBoundException e) {
		e.printStackTrace();
	}
}
}

