/**
 * 
 */
package types;

import java.util.Arrays;

/**
 * @author Vlad
 *
 */
public abstract class Sequence<T> extends Packet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected T[] buffer;
	
	/**
	 * @param device
	 * @param description
	 * @param date
	 */

	protected Sequence(String device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.buffer = buffer;
	}
	
//	protected Sequence(String device, String description, long date, int channelNr, String unit, double resolution,
//			int len) {
//		super(device, description, date);
//		this.channelNr = channelNr;
//		this.unit = unit;
//		this.resolution = resolution;
//		this.buffer = (T[]) new Object[len];
//	}

	@Override
	public String toString() {
		return "Sequence [channelNr=" + channelNr + ", unit=" + unit + ", resolution=" + resolution + ", buffer="
				+ Arrays.toString(buffer) + ", device=" + device + ", description=" + description + ", date=" + date
				+ "]";
	}

}
