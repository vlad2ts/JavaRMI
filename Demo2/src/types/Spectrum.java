package types;

import java.util.Arrays;

public class Spectrum<T> extends Sequence<T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3288941135301545933L;
	protected int scaling;
	
	public Spectrum(String device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer, int scaling) {
		super(device, description, date, channelNr, unit, resolution, buffer);
		this.scaling = scaling;
	}

	@Override
	public String toString() {
		return "Spectrum [scaling=" + scaling + ", channelNr=" + channelNr + ", unit=" + unit + ", resolution="
				+ resolution + ", buffer=" + Arrays.toString(buffer) + ", device=" + device + ", description="
				+ description + ", date=" + date + "]";
	}
}
