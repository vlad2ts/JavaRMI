/**
 * 
 */
package types;

import java.io.Serializable;

/**
 * @author Piter
 *
 */
public abstract class Packet implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String device;
	protected String description;
	protected long date;
	
	
	public Packet(String device, String description, long date) {
		this.device = device;
		this.description = description;
		this.date = date;
	}


	@Override
	public String toString() {
		return "Packet [device=" + device + ", description=" + description + ", date=" + date + "]";
	}
	
	public String getDevice() {
		return this.device;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public long getDate() {
		return this.date;
	}
}
