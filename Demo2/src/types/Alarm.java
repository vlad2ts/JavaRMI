package types;

public class Alarm extends Packet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int channelNr;
	protected int threshold;
	protected byte direction;

	public Alarm(String device, String description, long date, int channelNr, int threshold, byte direction) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.threshold = threshold;
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Alarm [channelNr=" + channelNr + ", threshold=" + threshold + ", direction=" + direction + ", device="
				+ device + ", description=" + description + ", date=" + date + "]";
	}
	

}
