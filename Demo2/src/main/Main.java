package main;

import types.*;

public class Main {

	public static void main(String[] args) {
//		Main m = new Main();
		
		// Initialize data
		String device = "deviceTest";
		String description = "descriptionTest";
		long date = 10250045;
		int channelNr = 3;
		String unit = "unitTest";
		double resolution = 0.01;
		double sensitivity = 0.1;
		int threshold = 50;
		byte direction = -1;
		Integer[] buffer = {1,2,3};
		int scaling = 0;
		
		//Create instances of classes with data
		TimeHistory<Integer> timeHistoryTest = new TimeHistory<Integer>(device, description, date, channelNr, unit, resolution,
				buffer, sensitivity);
		Spectrum<Integer> spectrumTest = new Spectrum<Integer>(device, description, date, channelNr, unit, resolution, buffer, scaling);
		Alarm alarmTest = new Alarm(device, description, date, channelNr, threshold, direction);
		
		//Print test instances' content
		System.out.println(timeHistoryTest.toString());
		System.out.println(spectrumTest.toString());
		System.out.println(alarmTest.toString());
	}

}
