package main;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import types.Spectrum;
import types.TimeHistory;

public class Servant extends UnicastRemoteObject 
implements Send {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, Spectrum<Integer>> SpectrumMap = new HashMap<String, Spectrum<Integer>>();
	private Map<String, TimeHistory<Integer>> TimeHistoryMap = new HashMap<String, TimeHistory<Integer>>();
	public Servant() throws RemoteException{
		
	} 
	public void SendSpectrum (Spectrum<Integer> a, String t) throws RemoteException{
		SpectrumMap.put(t,a); 
		System.out.println("Object Spectrum saved");
	}
	public void  SendTimeHistory (TimeHistory<Integer> a, String t) throws RemoteException{
		TimeHistoryMap.put(t, a);
		System.out.println("Object TimeHistory saved");
	}
	
	public void PrintTimeHistory(String key) throws RemoteException{
		TimeHistory<Integer> obj= TimeHistoryMap.get(key);
		System.out.println(obj.toString());
	}
    public void PrintSpectrum(String key) throws RemoteException{
    	Spectrum<Integer> obj = SpectrumMap.get(key);
    	System.out.println(obj.toString());
    }
}
