package main;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
	Registry reg; // rejestr nazw obiektow
	Servant servant; // klasa uslugowa
	
	public static void main(String[] args) {
		try {
		  new Server();
		} catch (Exception e) {
			e.printStackTrace(); 
			System.exit(1);
	} }
	
	protected Server() throws RemoteException {
		try { 
		  reg = LocateRegistry.getRegistry(); // Utworzenie rejestru nazw  
		  servant = new Servant();        // utworzenie zdalnego obiektu	
		  reg.rebind("Server", servant);  // zwiazanie nazwy z obiektem
		  System.out.println("Server READY");
		} catch(RemoteException e) {
		  e.printStackTrace(); 
		  throw e;
	}	}
}

